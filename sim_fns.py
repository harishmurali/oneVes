import numpy as np
from numpy.fft import ifft, fft

def tayGreen(x, y):
    return np.array([(np.sin(x)*np.cos(y)).T, (-np.cos(x)*np.sin(y)).T]).flatten()

def arcDeriv(f,m,sa,IK):
    f = f.flatten()
    for j in xrange(m):
        f = sa*np.real(ifft(IK*fft(f)))
    return f

def getState(x, y, kb, getVel=tayGreen, inp_size=64):
    ''' Return x, y, normal, bending force, curvature, velocity in the required format'''

    IK = 1j * np.array(range(0, inp_size/2)+ [0] + range(-inp_size/2+1, 0))
    x = np.reshape(x, (64))
    y = np.reshape(y, (64))
    Dx = np.real(ifft(IK * fft(x)))
    Dy = np.real(ifft(IK * fft(y)))
    isa = 1 / (Dx**2 + Dy**2)**0.5 
    ny, nx = -Dx * isa, Dy * isa

    state = {'nx': np.reshape(nx, [-1, 1, inp_size])}
    state['ny'] = np.reshape(ny, [-1, 1, inp_size])
    
    bx = -kb * arcDeriv(x, 4, isa, IK)
    by = -kb * arcDeriv(y, 4, isa, IK)
    state['bx'] = np.reshape(bx, [-1, 1, inp_size])
    state['by'] = np.reshape(by, [-1, 1, inp_size])

    DDx = arcDeriv(Dx, 1, np.ones_like(Dx), IK);
    DDy = arcDeriv(Dy, 1, np.ones_like(Dy), IK);
    curvature = (Dx*DDy - Dy*DDx)*(isa**3);
    state['curvature'] = np.reshape(curvature, [-1, 1, inp_size])

    x_re = np.reshape(x, [-1, 1, inp_size])
    y_re = np.reshape(y, [-1, 1, inp_size])
    state['x'] = x_re
    state['y'] = y_re

    v = np.zeros((x_re.shape[0], inp_size, 50))
    for k in range(x_re.shape[0]):
        it = 0
        for i,j in zip(x_re[k, 0, :], y_re[k, 0, :]):
            X,Y = np.meshgrid(np.linspace(i-0.25, i+0.25, 5), np.linspace(j-0.25, j+0.25, 5))
            v[k, it, :] = getVel(X, Y)
            it = it + 1
    state['v'] = v
    return state

