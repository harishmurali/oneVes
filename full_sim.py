from __future__ import division, print_function, absolute_import
import argparse

parser = argparse.ArgumentParser(description='Generate an autoencoder for given data')
parser.add_argument('-e', action="store", dest="training_epochs", type=int, default=1500, help="Number of training epochs (default 1500)")
parser.add_argument('-r', action="store", dest="alpha", type=float, default=0.0005, help="Regularisation coefficient (default 0.0005)")
parser.add_argument('-l', action="store", dest="learning_rate", type=float, default=0.01, help="Learning rate (default 0.01)")
parser.add_argument('-n', action="store",required=True, dest="res_n", help="label for output matrix (result_[n].mat)")
parser.add_argument('-b', action="store",required=False, dest="batch_size", type=int, default=64, help="minibatch size (defaults to 64)")

parser.add_argument('--length', action="store", dest="length", type=float, default=0.000001, help="Coefficient of length penalty(default 0.0005)")
parser.add_argument('--area', action="store", dest="area", type=float, default=0.001, help="Coefficient of area penalty(default 0.0005)")
parser.add_argument('--roughness', action="store", dest="roughness", type=float, default=5.0, help="Coefficient of roughness penalty(default 0.0005)")

parser.add_argument('--sim', action="store", dest="tsteps", type=int, default=-1, help="Number of time steps for simulation")
parser.add_argument('--angle', action="store", dest="angle", type=float, default=0.0, help="Angle for vesicle initialization")
parser.add_argument('-x', action="store", dest="x_c", type=float, default=0.0, help="x Center for vesicle initialization")
parser.add_argument('-y', action="store", dest="y_c", type=float, default=0.0, help="y Center for vesicle initialization")

parser.add_argument('--restart' , dest='restart' , default=False       , action='store_true')
parser.add_argument('--notrain' , dest='train'   , default=True        , action='store_false')
parser.add_argument('--log'     , dest='log'     , default=False       , action='store_true')
parser.add_argument('--data'    , action="store" , dest="inp_filename", required=False , default="tayGreenVF25Finer",
                    help="Filename of .mat datafile. Defaults to 'inp'. Format: {x:xdata , y:ydata}")
parser.parse_args()

import sys
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
import pdb
import os
import shutil
from data_handler import DataWrapper
import tensorflow as tf
from timeit import default_timer as timer
import csv
from tensorflow.python.ops.init_ops import glorot_uniform_initializer
#from sim_fns import getState, tayGreen
from tf_sim_fns import *
p = 4 #numchannels
w = 5 #window

restart = parser.parse_args().restart
res_n   = parser.parse_args().res_n

if restart:
    try:
        os.remove('costs'+res_n+'.csv')
        shutil.rmtree('timelySave'+res_n)
    except:
        print("One or more of costs[n].csv, result[n].mat, timelySave[n] missing")

train           = parser.parse_args().train
tsteps          = parser.parse_args().tsteps
x_c             = parser.parse_args().x_c
y_c             = parser.parse_args().y_c
angle           = parser.parse_args().angle
alpha           = parser.parse_args().alpha
learning_rate   = parser.parse_args().learning_rate
training_epochs = parser.parse_args().training_epochs
save_to         = "result" + res_n
regularizer     = tf.contrib.layers.l2_regularizer(alpha)
batch_size      = parser.parse_args().batch_size
log             = parser.parse_args().log
inp_filename    = parser.parse_args().inp_filename
data            = DataWrapper(inp_filename)

inp_size     = 64

x            = tf.placeholder('float32', shape=(None, 1, inp_size))
y            = tf.placeholder('float32', shape=(None, 1, inp_size))
# bx           = tf.placeholder('float32', shape=(None, 1, inp_size))
# by           = tf.placeholder('float32', shape=(None, 1, inp_size))
# nx           = tf.placeholder('float32', shape=(None, 1, inp_size))
# ny           = tf.placeholder('float32', shape=(None, 1, inp_size))
# curvature    = tf.placeholder('float32', shape=(None, 1, inp_size))
# velocity     = tf.placeholder('float32', shape=(None, inp_size, 50))


xCorr = tf.placeholder('float32', shape=(None, inp_size))
yCorr = tf.placeholder('float32', shape=(None, inp_size))

xCorr = xCorr - tf.reduce_mean(x,axis=2)#,keep_dims=True)
yCorr = yCorr - tf.reduce_mean(y,axis=2)#,keep_dims=True)


x = x - tf.reduce_mean(x,axis=2,keep_dims=True)
y = y - tf.reduce_mean(y,axis=2,keep_dims=True)

xCor = tf.reshape(xCorr, (-1, inp_size, 1))
yCor = tf.reshape(yCorr, (-1, inp_size, 1))
def fc(input, out_size=inp_size,regularizer=regularizer, kernel_initializer=tf.zeros_initializer(), format='CH_LAST', name=None, reuse=None):
    if format == 'CH_FIRST':
        fc1 = tf.layers.dense(input, out_size, kernel_regularizer=regularizer, kernel_initializer=tf.zeros_initializer(), name=name, reuse=reuse)
    elif format == 'CH_LAST':
        fc1 = tf.transpose(input, perm=[0, 2, 1])
        fc1 = tf.layers.dense(input, out_size, kernel_regularizer=regularizer, kernel_initializer=tf.zeros_initializer(), name=name, reuse=reuse)
    else:
        assert False
    fc1 = tf.transpose(fc1, perm=[0, 2, 1])
    return tf.maximum(fc1, -0.01 * fc1)


def convpool(input, out_layers, kernelwidth, pool_size=None, regularizer=regularizer, reuse=None, name=None):
    conv = tf.layers.conv1d(input, out_layers, kernelwidth, kernel_regularizer=regularizer, kernel_initializer=tf.zeros_initializer(), padding='same', name=name, reuse=reuse)
    res = tf.maximum(conv, -0.01*conv)
    if pool_size != None:
        res = tf.layers.max_pooling1d(res, pool_size, pool_size)
    return res

def step_graph(x, y, num_steps=1, reuse=False):
    for i in xrange(num_steps):

        state        = getState(x,y,5e-3)
        bx           = state['bx']
        by           = state['by']
        nx           = state['nx']
        ny           = state['ny']
        curvature    = state['curvature']
        velocity     = state['v']

        xy     = tf.concat([fc(x, inp_size * 2, format='CH_FIRST', reuse=reuse, name="xy_1"), fc(y, inp_size * 2, format='CH_FIRST', reuse=reuse, name="xy_2")], axis=2)
        xy     = convpool(xy, p, w, 2, reuse=reuse, name="xy_3")
        bforce = tf.concat([fc(bx, inp_size * 2, format='CH_FIRST', reuse=reuse, name="bf_1"), fc(by, inp_size * 2, format='CH_FIRST', reuse=reuse, name="bf_2")],axis=2)
        bforce = convpool(bforce, p, w, 2, reuse=reuse, name="bf")
        normal = tf.concat([fc(nx, inp_size * 2, format='CH_FIRST', reuse=reuse, name="n_1"), fc(ny, inp_size * 2, format='CH_FIRST', reuse=reuse, name="n_2")], axis=2)
        normal = convpool(normal, p, w, 2, reuse=reuse, name="n")
        curv   = convpool(fc(curvature, inp_size * 2, format='CH_FIRST', reuse=reuse, name="curv1"), p, w, 2, reuse=reuse, name="curv")
        conv1  = convpool(tf.concat([xy, bforce, curv, normal], axis=2), p, w, reuse=reuse, name="conv1")
        
        result = []
        for i in xrange(inp_size):
            unrolled = tf.reshape(tf.concat([conv1[:, i, :], velocity[:, i, :]], axis=1), (-1, 1, 50+p))
            result.append(tf.layers.dense(unrolled, 2, kernel_regularizer=regularizer, kernel_initializer=tf.zeros_initializer(), reuse=reuse, name="unrolled"+str(i)))
        result = tf.concat(result, axis=1)
        [xDelta, yDelta] = tf.split(result, num_or_size_splits=2, axis=2)
        x     = x + tf.transpose(xDelta, [0, 2, 1])
        y     = y + tf.transpose(yDelta, [0, 2, 1])
        reuse = True
    return x, y

xNew, yNew = step_graph(x, y)
#xf10, yf10 = step_graph(xNew, yNew, num_steps=9, reuse=True)

IK       = np.fft.fftfreq(inp_size)*1j
IK       = IK.astype(np.dtype('complex64'))
dbydx    = tf.real(tf.ifft(tf.multiply(IK,tf.fft(tf.complex(xNew, 0.0)))))
dbydy    = tf.real(tf.ifft(tf.multiply(IK,tf.fft(tf.complex(yNew, 0.0)))))
length   = tf.reduce_sum(tf.sqrt(tf.add(tf.square(dbydx),tf.square(dbydy))), 1)
area     = tf.reduce_sum(tf.add(tf.multiply(xNew,dbydy),-1*tf.multiply(yNew,dbydx)), 1)

r_dbydx    = tf.real(tf.ifft(tf.multiply(IK,tf.fft(tf.complex(xCor, 0.0)))))
r_dbydy    = tf.real(tf.ifft(tf.multiply(IK,tf.fft(tf.complex(yCor, 0.0)))))
r_length   = tf.reduce_sum(tf.sqrt(tf.add(tf.square(r_dbydx),tf.square(r_dbydy))), 1)
r_area     = tf.reduce_sum(tf.add(tf.multiply(xCor,r_dbydy),-1*tf.multiply(yCor,r_dbydx)), 1)

l2       = tf.add_n([tf.reduce_sum(tf.pow(xNew - xCor, 2)), tf.reduce_sum(tf.pow(yNew - yCor, 2))])
c2       = parser.parse_args().roughness*tf.add_n([tf.reduce_sum(tf.square(dbydx)), tf.reduce_sum(tf.square(dbydy))])
c3       = parser.parse_args().length*tf.reduce_sum(tf.pow(length-r_length,2))
c4       = parser.parse_args().area*tf.reduce_sum(tf.pow((area-r_area)/r_area,2)) #1e-2
reg_term = tf.add_n(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))

cost     = tf.add_n([l2 ,reg_term])


global_step = tf.Variable(0, name='global_step', trainable=False)

optimizer = tf.train.RMSPropOptimizer(learning_rate).minimize(cost, global_step=global_step)


class saveHook(tf.train.SessionRunHook):
    def begin(self):
        self.train_time = timer()
    def after_run(self, run_context, fuckit):
        sess = run_context.session
        batch = data.getBatch(batch_size)
        # batch = sio.loadmat(inp_filename)
        if global_step.eval(session=sess)%5000 == 0:
            y_pred, x_pred, cst, c_l2 = sess.run([yNew, xNew, cost, l2], feed_dict={x: batch["x"],y: batch["y"], xCorr:batch['xCorr'], yCorr: batch['yCorr']})
            tosave = {  'x_pred': x_pred,
                        'y_pred': y_pred,
                        'x_act' : batch['xCorr'],
                        'y_act' : batch['yCorr'],
                        'learning_rate': learning_rate,
                        'training_epochs':training_epochs,
                        'alpha_reg': alpha,
                        'l2_loss': c_l2,
                        'costs':cst,
                        }
            sio.savemat("live" + str(parser.parse_args().res_n) + ".mat",tosave)
    def end(self, sess):
        self.train_time = timer() - self.train_time
        batch = data.getBatch(batch_size)
        # batch = sio.loadmat(inp_filename)
        y_pred, x_pred, cst, c_l2 = sess.run([yNew, xNew, cost, l2], feed_dict={x: batch["x"],y: batch["y"], xCorr:batch['xCorr'], yCorr: batch['yCorr']})
        tosave = {  'x_pred': x_pred,
                    'y_pred': y_pred,
                    'x_act' : batch['xCorr'],
                    'y_act' : batch['yCorr'],
                    'learning_rate': learning_rate,
                    'training_epochs':training_epochs,
                    'train_time': self.train_time,
                    'alpha_reg': alpha,
                    'l2_loss': costs_l2,
                    'costs':costs,}

        sio.savemat(save_to+".mat",tosave)

        # batch = sio.loadmat("kb1_v3.mat")
        batch = data.getValidationData()
        val_size = len(batch["xCorr"])
        start = timer()
        y_pred, x_pred, cst_val, v_l2 = sess.run([yNew, xNew, cost, l2], feed_dict={x: batch["x"],y: batch["y"], xCorr:batch['xCorr'], yCorr: batch['yCorr']})
        val_time = timer() - start

        tosave = {  'x_pred': x_pred,
                    'y_pred': y_pred,
                    'x_act' : batch['xCorr'],
                    'y_act' : batch['yCorr'],
                    'x_initial': batch['x'],
                    'y_initial': batch['y'],
                    'total_cost':cst_val,
                    'val_time': val_time,
                    'l2_loss': v_l2,
                    }
        sio.savemat("validation" + str(parser.parse_args().res_n) + ".mat",tosave)

        '''
        batch = # data.getTestData()
        batch = sio.loadmat(inp_filename)
        yCor, xCor, cost, c_l2 = sess.run([yCorrPred, xCorrPred, cost, l2], feed_dict={x: batch["x"],y: batch["y"], xCorr:batch['xCorr'], yCorr: batch['yCorr']})
        tosave = {  'x_act' : batch['main'][:, 0, :],
                    'y_act' : batch['main'][:, 1, :],
                    'x_pred':xCorr,
                    'y_pred':yCorr,
                    'total_cost':cost,
                    'l2_loss': c_l2,
                    }
        sio.savemat("test" + str(parser.parse_args().res_n) + ".mat",tosave)
        '''
        if log:
            if not os.path.exists('log.csv'):
                with open('log.csv', 'wb') as logfile:
                    logger = csv.writer(logfile, delimiter=',')
                    logger.writerow(['Run no.'         , 'Number of Epochs'   , 'Batch_size'    , 'Regularisation ceoff' ,
                                     'Learning Rate'  , 
                                     'Validation time' , 'Cost on validation' , 'Cost on train' , 'Train time'           ,
                                     'L2 error on training', 'L2 error on validation'])
            with open('log.csv', 'ab') as logfile:
                logger = csv.writer(logfile, delimiter=',')
                logger.writerow([parser.parse_args().res_n , sess.run(global_step)      , batch_size               , alpha                         ,
                                  learning_rate            ,
                                  val_time                 , cst_val/val_size           , costs[-1]/batch_size     , self.train_time               ,
                                  costs_l2[-1]/batch_size  , v_l2/val_size])

hooks=[tf.train.StopAtStepHook(num_steps=training_epochs), saveHook()]
costs = []
costs_l2 = []

f = open('costs'+str(res_n)+'.csv', 'a')
restore = True
if train:
    with tf.train.MonitoredTrainingSession(checkpoint_dir="./timelySave"+str(parser.parse_args().res_n)+"/",
                                           hooks=hooks) as mon_sess:
    
    
        tf.set_random_seed(1)
        while not mon_sess.should_stop():
            batch = data.getBatch(batch_size)
            #batch = sio.loadmat(inp_filename)
            _, cst, l2_loss, gs =  mon_sess.run([optimizer, cost, l2, global_step], feed_dict={x: batch["x"], y: batch["y"],xCorr:batch['xCorr'], yCorr: batch['yCorr']})
            costs.append(cst)
            costs_l2.append(l2_loss)
            f.write(str(cst)+','+str(l2_loss)+"\n")
    print("Optimization Finished!")
train = False
hooks = []
if tsteps != -1:
    with tf.train.MonitoredTrainingSession(checkpoint_dir="./timelySave"+str(res_n)+"/",
                                           hooks=hooks) as sim_sess:

        X = sio.loadmat('relaxed64.mat')['X']
        Xin = np.zeros_like(X);

        Xin[:inp_size] = X[:inp_size]*np.cos(angle) - X[inp_size:]*np.sin(angle);
        Xin[inp_size:] = X[:inp_size]*np.sin(angle) + X[inp_size:]*np.cos(angle);

        xSim = np.zeros((tsteps, inp_size))
        ySim = np.zeros((tsteps, inp_size))
        xSim[0, :] = (Xin[:inp_size] - np.mean(Xin[:inp_size])).T + x_c
        ySim[0, :] = (Xin[inp_size:] - np.mean(Xin[inp_size:])).T + y_c
        for it in xrange(tsteps-1):
            xSim[it+1, :], ySim[it+1, :] = sim_sess.run([xNew, yNew], feed_dict={x: np.reshape(xSim[it, :], [-1, 1, inp_size]), y: np.reshape(ySim[it, :], [-1, 1, inp_size])})
        sio.savemat("sim" + str(res_n) + ".mat", {'x': xSim, 'y': ySim})
        print("Simulation Finished")

f.close()

