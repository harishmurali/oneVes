import scipy.io as sio
import numpy as np
import pickle

class DataWrapper(object):
    def __init__(self, filename="kb1", batch_size=32, liveSaveFile="live.pkl", savefile="result.pkl"):
        with open(filename+"_train.pkl", 'rb') as f:
            data = pickle.load(f)
            self.x         = data["x"]
            self.y         = data["y"]
            self.bx        = data["bx"]
            self.by        = data["by"]
            self.nx        = data["nx"]
            self.ny        = data["ny"]
            self.xCorr     = data["xCorr"]
            self.yCorr     = data["yCorr"]
            self.curvature = data["curvature"]
            self.v         = data["v"]

        with open(filename+"_val.pkl",'rb') as f:
            data = pickle.load(f)
            self.vx         = data["x"]
            self.vy         = data["y"]
            self.vbx        = data["bx"]
            self.vby        = data["by"]
            self.vnx        = data["nx"]
            self.vny        = data["ny"]
            self.vxCorr     = data["xCorr"]
            self.vyCorr     = data["yCorr"]
            self.vcurvature = data["curvature"]
            self.vv         = data["v"]

        with open(filename+"_test.pkl",'rb') as f:
            data = pickle.load(f)
            self.tx         = data["x"]
            self.ty         = data["y"]
            self.tbx        = data["bx"]
            self.tby        = data["by"]
            self.tnx        = data["nx"]
            self.tny        = data["ny"]
            self.txCorr     = data["xCorr"]
            self.tyCorr     = data["yCorr"]
            self.tcurvature = data["curvature"]
            self.tv         = data["v"]

        self.filename = filename
        self.batch_size = batch_size
        self.liveSaveFile = liveSaveFile
        self.savefile = savefile

    def getTestData(self):
        return {"x"         : self.tx,
                "y"         : self.ty,
                "bx"        : self.tbx,
                "by"        : self.tby,
                "nx"        : self.tnx,
                "ny"        : self.tny,
                "xCorr"     : self.txCorr,
                "yCorr"     : self.tyCorr,
                "curvature" : self.tcurvature,
                "v"         : self.tv,
                }
    def getValidationData(self):
        return {"x"         : self.vx,
                "y"         : self.vy,
                "bx"        : self.vbx,
                "by"        : self.vby,
                "nx"        : self.vnx,
                "ny"        : self.vny,
                "xCorr"     : self.vxCorr,
                "yCorr"     : self.vyCorr,
                "curvature" : self.vcurvature,
                "v"         : self.vv,
                }

    def getBatch(self, batch_size=None):
        if batch_size is None:
            batch_size = self.batch_size
        try:
            sample = np.random.randint(0,high=len(self.x), size=batch_size)
            return {"x"         : self.x[sample],
                    "y"         : self.y[sample],
                    "bx"        : self.bx[sample],
                    "by"        : self.by[sample],
                    "nx"        : self.nx[sample],
                    "ny"        : self.ny[sample],
                    "xCorr"     : self.xCorr[sample],
                    "yCorr"     : self.yCorr[sample],
                    "curvature" : self.curvature[sample],
                    "v"         : self.v[sample]
                   }
        except:
            import pdb; pdb.set_trace()

