import numpy as np
import scipy.io as sio

filenames = []

with open('ves_sims.csv','rb') as f:
    for line in f.readlines():
        filenames.append(line.split('.')[0])
outfile = "oneVes_rand"
dicts = []

for filename in filenames:
    dicts.append(sio.loadmat(filename+".mat"))
    dicts[-1].pop('__header__', None)
    dicts[-1].pop('__globals__', None)
    dicts[-1].pop('__version__', None)

data = dicts[0]

for key in data:
    for d in dicts[1:]:
        data[key] = np.append(data[key], d[key], axis=0)
data['kb'] = data['kb'].T[:-10]

inpsz = data[data.keys()[0]].shape[0]
tenpercent = inpsz/10
np.random.seed(217132)
Epochs   = np.random.choice(xrange(inpsz), tenpercent, replace=False)

Validation = {}

for key in data:
    Validation[key] = data[key][Epochs][:][:]
    data[key] = np.delete(data[key], Epochs, 0)

sio.savemat(outfile + "_val.mat",Validation)

np.random.seed(777777)
inpsz = data[data.keys()[0]].shape[0]
Epochs   = np.random.choice(xrange(inpsz), tenpercent, replace=False)
Test = {}
for key in data:
    Test[key] = data[key][Epochs][:][:]
    data[key] = np.delete(data[key], Epochs, 0)

sio.savemat(outfile + "_test.mat",Test)


Train = {}
for key in data:
    Train[key] = data[key]

sio.savemat(outfile + "_train.mat",Train)

