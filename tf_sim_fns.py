import numpy as np
import tensorflow as tf
import pdb
from numpy.fft import ifft, fft

def tayGreen(x, y):
    return tf.concat([(tf.sin(x)*tf.cos(y)), (-tf.cos(x)*tf.sin(y))], axis=3)

def arcDeriv(f,m,sa,IK, inp_size):
    f = tf.reshape(f, [-1, inp_size])
    for j in xrange(m):
        f = sa*tf.real(tf.ifft(IK*tf.fft(tf.complex(f,0.0))))
    return f

def getState(x, y, kb, getVel=tayGreen, inp_size=64):
    ''' Return x, y, normal, bending force, curvature, velocity in the required format'''

    IK = 1j * np.array(range(0, inp_size/2)+ [0] + range(-inp_size/2+1, 0))
    IK = IK.astype(np.dtype('complex64'))
    x = tf.reshape(x, (-1, 64))
    y = tf.reshape(y, (-1, 64))
    Dx = tf.real(tf.ifft(tf.multiply(IK,tf.fft(tf.complex(x,0.0)))))
    Dy = tf.real(tf.ifft(tf.multiply(IK,tf.fft(tf.complex(y,0.0)))))
    isa = tf.div(1.0,tf.sqrt(tf.square(Dx) + tf.square(Dy)))
    ny, nx = -Dx * isa, Dy * isa

    state = {'nx': tf.reshape(nx, [-1, 1, inp_size])}
    state['ny'] = tf.reshape(ny, [-1, 1, inp_size])
    
    bx = -kb * arcDeriv(x, 4, isa, IK, inp_size)
    by = -kb * arcDeriv(y, 4, isa, IK, inp_size)
    state['bx'] = tf.reshape(bx, [-1, 1, inp_size])
    state['by'] = tf.reshape(by, [-1, 1, inp_size])

    DDx = arcDeriv(Dx, 1, tf.ones_like(Dx), IK, inp_size)
    DDy = arcDeriv(Dy, 1, tf.ones_like(Dy), IK, inp_size)
    curvature = (Dx*DDy - Dy*DDx)*(tf.pow(isa,3))
    state['curvature'] = tf.reshape(curvature, [-1, 1, inp_size])

    x_re = tf.reshape(x, [-1, 1, inp_size])
    y_re = tf.reshape(y, [-1, 1, inp_size])
    state['x'] = x_re
    state['y'] = y_re

    x_v = tf.reshape(x,[-1,64,1,1])
    y_v = tf.reshape(y,[-1,64,1,1])
    term = tf.reshape(tf.linspace(-0.25,0.25,5),[1,1,5,1])
    x_v = tf.zeros([1,64,5,5]) + (x_v + term)
    y_v = tf.transpose(tf.zeros([1,64,5,5]) + (y_v + term),[0,1,3,2])
    
    v = tf.reshape(tayGreen(x_v,y_v), (-1, inp_size, 50))
    #v = tf.zeros((None, inp_size, 50))
    
    #for k in range(x_re.shape[0]):
    #    it = 0
    #    for i,j in zip(x_re[k, 0, :], y_re[k, 0, :]):
    #        X,Y = np.meshgrid(np.linspace(i-0.25, i+0.25, 5), np.linspace(j-0.25, j+0.25, 5))
    #        v[k, it, :] = getVel(X, Y)
    #        it = it + 1
    state['v'] = v
    return state

