#!/bin/bash
#SBATCH -J oneVes           # job name
#SBATCH -o 1zz.o       # output and error file name (%j expands to jobID)
#SBATCH -N 1              # total number of mpi tasks requested
#SBATCH -n 1              # total number of mpi tasks requested
#SBATCH -p gpu     # queue (partition) -- normal, development, etc.
#SBATCH -t 02:30:00        # run time (hh:mm:ss) - 1.5 hours
n=101
python main.py -e200 -l1e-8 -n$n -b128 --data run9 -r 1e-7 --length 100 --area 100 --restart
scp -P 8704 ./costs$n.csv ./result$n.mat ./validation$n.mat ./log_val$n.mat harish@neymar.ices.utexas.edu:~/oneVes_data/
