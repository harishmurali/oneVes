from __future__ import division, print_function, absolute_import
import scipy
import argparse

parser = argparse.ArgumentParser(description='Generate an autoencoder for given data')
parser.add_argument('-e', action="store", dest="training_epochs", type=int, default=1500, help="Number of training epochs (default 1500)")
parser.add_argument('-r', action="store", dest="alpha", type=float, default=50.0, help="Regularisation coefficient (default 50)")
parser.add_argument('-l', action="store", dest="learning_rate", type=float, default=0.01, help="Learning rate (default 0.01)")
parser.add_argument('-n', action="store",required=True, dest="res_n", help="label for output matrix (result_[n].mat)")
parser.add_argument('-b', action="store",required=False, dest="batch_size", type=int, default=64, help="minibatch size (defaults to 64)")

parser.add_argument('--length', action="store", dest="length", type=float, default=0.000001, help="Coefficient of length penalty(default 0.0005)")
parser.add_argument('--area', action="store", dest="area", type=float, default=0.001, help="Coefficient of area penalty(default 0.0005)")
parser.add_argument('--roughness', action="store", dest="roughness", type=float, default=5.0, help="Coefficient of roughness penalty(default 0.0005)")

parser.add_argument('--sim', action="store", dest="tsteps", type=int, default=-1, help="Number of time steps for simulation")
parser.add_argument('--angle', action="store", dest="angle", type=float, default=0.0, help="Angle for vesicle initialization")
parser.add_argument('-x', action="store", dest="x_c", type=float, default=0.0, help="x Center for vesicle initialization")
parser.add_argument('-y', action="store", dest="y_c", type=float, default=0.0, help="y Center for vesicle initialization")

parser.add_argument('--restart' , dest='restart' , default=False       , action='store_true')
parser.add_argument('--log'     , dest='log'     , default=False       , action='store_true')
parser.add_argument('--data'    , action="store" , dest="inp_filename" , required=False       , default="tayGreenVF25Finer" , help="Filename of .mat datafile. Defaults to 'inp'. Format: {x:xdata , y:ydata}")
parser.parse_args()

import sys
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
import pdb
import os
import shutil
from data_handler import DataWrapper
import tensorflow as tf
from timeit import default_timer as timer
import csv
from tensorflow.python.ops.init_ops import glorot_uniform_initializer
from sim_fns import *

p = 6 #numchannels
w = 17 #window

restart = parser.parse_args().restart

if restart:
    try:
        os.remove('costs'+parser.parse_args().res_n+'.csv')
    except:
        print("One or more of costs[n].csv, result[n].mat, timelySave[n] missing")
    try:
        shutil.rmtree('timelySave'+parser.parse_args().res_n)
    except:
        print("One or more of costs[n].csv, result[n].mat, timelySave[n] missing")
    try:
        os.remove('log_val'+parser.parse_args().res_n+'.csv')
    except:
        print("One or more of costs[n].csv, result[n].mat, timelySave[n] missing")

alpha           = parser.parse_args().alpha
learning_rate   = parser.parse_args().learning_rate
training_epochs = parser.parse_args().training_epochs
save_to         = "result" + parser.parse_args().res_n
regularizer     = tf.contrib.layers.l2_regularizer(alpha)
batch_size      = parser.parse_args().batch_size
log             = parser.parse_args().log
inp_filename    = parser.parse_args().inp_filename
data            = DataWrapper(inp_filename)

tsteps          = parser.parse_args().tsteps
x_c             = parser.parse_args().x_c
y_c             = parser.parse_args().y_c
angle           = parser.parse_args().angle

inp_size     = 64
x            = tf.placeholder('float32', shape=(None, 1, inp_size))
y            = tf.placeholder('float32', shape=(None, 1, inp_size))
bx           = tf.placeholder('float32', shape=(None, 1, inp_size))
by           = tf.placeholder('float32', shape=(None, 1, inp_size))
nx           = tf.placeholder('float32', shape=(None, 1, inp_size))
ny           = tf.placeholder('float32', shape=(None, 1, inp_size))
curvature    = tf.placeholder('float32', shape=(None, 1, inp_size))
velocity     = tf.placeholder('float32', shape=(None, inp_size, 50))


xCorr  = tf.placeholder('float32', shape=(None, inp_size))
yCorr  = tf.placeholder('float32', shape=(None, inp_size))

xCor   = xCorr - tf.reduce_mean(x,axis=2)#,keep_dims=True)
yCor   = yCorr - tf.reduce_mean(y,axis=2)#,keep_dims=True)

x_cent = x - tf.reduce_mean(x,axis=2,keep_dims=True)
y_cent = y - tf.reduce_mean(y,axis=2,keep_dims=True)

def fc(input, out_size=inp_size,regularizer=regularizer, kernel_initializer=glorot_uniform_initializer, format='CH_LAST', outp_format='CH_LAST',name="unique",reuse=False):
    if format == 'CH_FIRST':
        fc1 = tf.layers.dense(input, out_size, kernel_regularizer=regularizer, kernel_initializer=glorot_uniform_initializer(seed=0),name=name,reuse=reuse)
    elif format == 'CH_LAST':
        fc1 = tf.transpose(input, perm=[0, 2, 1])
        fc1 = tf.layers.dense(fc1, out_size, kernel_regularizer=regularizer, kernel_initializer=glorot_uniform_initializer(seed=0),name=name,reuse=reuse)
    else:
        assert False
    if outp_format=="CH_LAST":
        fc1 = tf.transpose(fc1, perm=[0, 2, 1])
    return tf.maximum(fc1, -0.01 * fc1)
    #return fc1


def convpool(input, out_layers, kernelwidth, pool_size=None, regularizer=regularizer):
    pre   = tf.slice(input, [0,0,0],[-1,int(w/2),-1])
    suf   = tf.slice(input, [0,inp_size-1-int(w/2),0],[-1,int(w/2),-1])
    input = tf.concat([suf,input,pre],1)
    conv = tf.layers.conv1d(input, out_layers, kernelwidth, kernel_regularizer=regularizer, kernel_initializer=glorot_uniform_initializer(seed=0), padding='valid')
    res = tf.maximum(conv, -0.01*conv)
    if pool_size != None:
        res = tf.layers.max_pooling1d(res, pool_size, pool_size)
    return res

xy     = tf.concat([tf.transpose(x_cent,(0,2,1)), tf.transpose(y_cent,(0,2,1))], axis=2)
xy     = convpool(xy, 2*p, w)
xy     = convpool(xy, p, w)
bforce = tf.concat([tf.transpose(bx,(0,2,1)), tf.transpose(by, (0,2,1))], axis=2)
bforce = convpool(bforce, 2*p, w)
bforce = convpool(bforce, p, w)
normal = tf.concat([tf.transpose(nx,(0,2,1)), tf.transpose(ny,(0,2,1))], axis=2)
normal = convpool(normal, p, w)
curv   = convpool(tf.transpose(curvature,(0,2,1)), p, w)
conv1  = convpool(tf.concat([xy, bforce, curv, normal], axis=2), 30*p, w)
conv1  = convpool(conv1, 10*p, w)
conv1  = convpool(conv1, 10*p, w)
conv1  = convpool(conv1, 10*p, w)
conv1  = convpool(conv1, 8*p, w)

result = []
for i in xrange(inp_size):
    unrolled    = tf.reshape(tf.concat([conv1[:, i, :], velocity[:, i, :]], axis=1), (-1, 1, 50+8*p))
    hidden_beg  = fc(unrolled,out_size=50+8*p, format='CH_FIRST',name='h1',reuse=(i!=0))
    hidden_mid  = fc(hidden_beg,out_size=50,name='h2',reuse=(i!=0))
    hidden_mid  = fc(hidden_mid,out_size=10,name='h3',reuse=(i!=0))
    hidden_mid  = fc(hidden_mid,out_size=10,name='h4',reuse=(i!=0))
    hidden_mid  = fc(hidden_mid,out_size=10,name='h5',reuse=(i!=0))
    hidden_mid  = fc(hidden_mid,out_size=10,name='h6',reuse=(i!=0))
    hidden_mid  = fc(hidden_mid,out_size=10,name='h7',reuse=(i!=0))
    hidden_end  = fc(hidden_mid,out_size= 5,name='h11',reuse=(i!=0), outp_format="CH_FIRST")
    result.append(tf.layers.dense(hidden_end,2,kernel_initializer=glorot_uniform_initializer(seed=0), kernel_regularizer=regularizer, name="final", reuse=(i!=0)))

result = tf.concat(result, axis=1)
result = tf.reshape(result, [-1,inp_size,2])
[xDelta, yDelta] = tf.split(result, num_or_size_splits=2, axis=2)
xNew     = tf.reshape(tf.transpose(x,[0,2,1]) + xDelta, [-1, inp_size])
yNew     = tf.reshape(tf.transpose(y,[0,2,1]) + yDelta, [-1, inp_size])
IK       = 1j * np.array(range(0, int(inp_size/2))+ [0] + range(-int(inp_size/2)+1, 0))
IK       = np.reshape(IK, [1, inp_size])
IK       = IK.astype(np.dtype('complex64'))
dbydx    = tf.real(tf.ifft(tf.multiply(IK,tf.fft(tf.complex(xNew, 0.0)))))
dbydy    = tf.real(tf.ifft(tf.multiply(IK,tf.fft(tf.complex(yNew, 0.0)))))
length   = tf.reduce_sum(tf.sqrt(tf.add(tf.square(dbydx),tf.square(dbydy))), 1)
area     = tf.reduce_sum(tf.add(tf.multiply(xNew,dbydy),-1*tf.multiply(yNew,dbydx)), 1)

r_dbydx    = tf.real(tf.ifft(tf.multiply(IK,tf.fft(tf.complex(xCorr, 0.0)))))
r_dbydy    = tf.real(tf.ifft(tf.multiply(IK,tf.fft(tf.complex(yCorr, 0.0)))))
r_length   = tf.reduce_sum(tf.sqrt(tf.add(tf.square(r_dbydx),tf.square(r_dbydy))), 1)
r_area     = tf.reduce_sum(tf.add(tf.multiply(xCorr,r_dbydy),-1*tf.multiply(yCorr,r_dbydx)), 1)

l2       = tf.add_n([tf.reduce_sum(tf.pow(xNew - xCorr, 2)), tf.reduce_sum(tf.pow(yNew - yCorr, 2))])
c2       = parser.parse_args().roughness*tf.add_n([tf.reduce_sum(tf.square(dbydx)), tf.reduce_sum(tf.square(dbydy))])
c3       = parser.parse_args().length*tf.reduce_sum(tf.pow(length-r_length,2))
c4       = parser.parse_args().area*tf.reduce_sum(tf.pow((area-r_area)/r_area,2)) #1e-2
reg_term = tf.add_n(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))

cost     = tf.add_n([l2 ,reg_term, c3, c4])

global_step = tf.Variable(0, name='global_step', trainable=False)

optimizer = tf.train.RMSPropOptimizer(learning_rate).minimize(cost, global_step=global_step)

class saveHook(tf.train.SessionRunHook):
    def begin(self):
        self.train_time = timer()
    def after_run(self, run_context, fuckit):
        sess = run_context.session
        batch = data.getValidationData()
        # batch = sio.loadmat(inp_filename)
        if global_step.eval(session=sess)%100 == 0:
            # y_pred, x_pred, cst, c_l2 = sess.run([yNew, xNew, cost, l2], feed_dict={x: batch["x"],y: batch["y"], xCorr:batch['xCorr'], yCorr: batch['yCorr'], nx: batch['nx'], ny: batch['ny'], bx: batch["bx"], by:batch["by"], curvature:batch["curvature"], velocity:batch["v"]})
            # tosave = {  'x_pred': x_pred,
            #             'y_pred': y_pred,
            #             'x_act' : batch['xCorr'],
            #             'y_act' : batch['yCorr'],
            #             'learning_rate': learning_rate,
            #             'training_epochs':training_epochs,
            #             'alpha_reg': alpha,
            #             'l2_loss': c_l2,
            #             'costs':cst,
            #             }
            # sio.savemat("live" + str(parser.parse_args().res_n) + ".mat",tosave)
            # batch = data.getValidationData()
            v_l2 = sess.run(l2, feed_dict={x: batch["x"],y: batch["y"], xCorr:batch['xCorr'], yCorr: batch['yCorr'], nx: batch['nx'], ny: batch['ny'], bx: batch["bx"], by:batch["by"], curvature:batch["curvature"], velocity:batch["v"]})
            with open('log_val'+parser.parse_args().res_n+'.csv', 'ab') as logfile:
                logger = csv.writer(logfile, delimiter=',')
                logger.writerow([v_l2])
    def end(self, sess):
        self.train_time = timer() - self.train_time
        batch = data.getBatch(batch_size)
        # batch = sio.loadmat(inp_filename)
        y_pred, x_pred, cst, c_l2 = sess.run([yNew, xNew, cost, l2], feed_dict={x: batch["x"],y: batch["y"], xCorr:batch['xCorr'], yCorr: batch['yCorr'], nx: batch['nx'], ny: batch['ny'], bx: batch["bx"], by:batch["by"], curvature:batch["curvature"], velocity:batch["v"]})
        tosave = {  'x_pred': x_pred,
                    'y_pred': y_pred,
                    'x_act' : batch['xCorr'],
                    'y_act' : batch['yCorr'],
                    'learning_rate': learning_rate,
                    'training_epochs':training_epochs,
                    'train_time': self.train_time,
                    'alpha_reg': alpha,
                    'l2_loss': costs_l2,
                    'costs':costs,}

        sio.savemat(save_to+".mat",tosave)

        # batch = sio.loadmat("kb1_v3.mat")
        batch = data.getValidationData()
        val_size = len(batch["xCorr"])
        start = timer()
        y_pred, x_pred, cst_val, v_l2 = sess.run([yNew, xNew, cost, l2], feed_dict={x: batch["x"],y: batch["y"], xCorr:batch['xCorr'], yCorr: batch['yCorr'], nx: batch['nx'], ny: batch['ny'], bx: batch["bx"], by:batch["by"], curvature:batch["curvature"], velocity:batch["v"]})
        val_time = timer() - start

        tosave = {  'x_pred': x_pred,
                    'y_pred': y_pred,
                    'x_act' : batch['xCorr'],
                    'y_act' : batch['yCorr'],
                    'x_initial': batch['x'],
                    'y_initial': batch['y'],
                    'total_cost':cst_val,
                    'val_time': val_time,
                    'l2_loss': v_l2,
                    }
        sio.savemat("validation" + str(parser.parse_args().res_n) + ".mat",tosave)

        '''
        batch = # data.getTestData()
        batch = sio.loadmat(inp_filename)
        yCor, xCor, cost, c_l2 = sess.run([yCorrPred, xCorrPred, cost, l2], feed_dict={x: batch["x"],y: batch["y"], xCorr:batch['xCorr'], yCorr: batch['yCorr'], nx: batch['nx'], ny: batch['ny'], bx: batch["bx"], by:batch["by"], curvature:batch["curvature"], velocity:batch["v"]})
        tosave = {  'x_act' : batch['main'][:, 0, :],
                    'y_act' : batch['main'][:, 1, :],
                    'x_pred':xCorr,
                    'y_pred':yCorr,
                    'total_cost':cost,
                    'l2_loss': c_l2,
                    }
        sio.savemat("test" + str(parser.parse_args().res_n) + ".mat",tosave)
        '''
        if log:
            if not os.path.exists('log.csv'):
                with open('log.csv', 'wb') as logfile:
                    logger = csv.writer(logfile, delimiter=',')
                    logger.writerow(['Run no.'       , 'Number of Epochs'     , 'Batch_size'               , 'Regularisation ceoff' ,
                                     'Learning Rate' , 'Validation time'      , 'Cost on validation'       , 'Cost on train'        ,
                                     'Train time'    , 'L2 error on training' , 'L2 error on validation'])
            with open('log.csv', 'ab') as logfile:
                logger = csv.writer(logfile, delimiter=',')
                logger.writerow([parser.parse_args().res_n , sess.run(global_step)   , batch_size       , alpha                ,
                                  learning_rate            , val_time                , cst_val/val_size , costs[-1]/batch_size ,
                                  self.train_time          , costs_l2[-1]/batch_size , v_l2/val_size])

hooks=[tf.train.StopAtStepHook(num_steps=training_epochs), saveHook()]


costs = []
costs_l2 = []
if training_epochs:
    f = open('costs'+str(parser.parse_args().res_n)+'.csv', 'a')
    restore = True
    with tf.train.MonitoredTrainingSession(checkpoint_dir="./timelySave"+str(parser.parse_args().res_n)+"/",
                                           hooks=hooks) as mon_sess:


        tf.set_random_seed(1)
        while not mon_sess.should_stop():
            batch = data.getBatch(batch_size)
            #batch = sio.loadmat(inp_filename)
            _, cst, l2_loss, gs =  mon_sess.run([optimizer, cost, l2, global_step], feed_dict={x: batch["x"], y: batch["y"],xCorr:batch['xCorr'], yCorr: batch['yCorr'],  nx: batch['nx'], ny: batch['ny'], bx: batch["bx"], by:batch["by"], curvature:batch["curvature"], velocity:batch["v"]})
            costs.append(cst)
            costs_l2.append(l2_loss)
            f.write(str(cst)+','+str(l2_loss)+"\n")
    print("Optimization Finished!")
    f.close()

train = False
hooks = []
res_n = parser.parse_args().res_n

if tsteps != -1:
    print('Starting sim')
    with tf.train.MonitoredTrainingSession(checkpoint_dir="./timelySave"+str(res_n)+"/",
                                           hooks=hooks) as sim_sess:

        start = timer()
        X = sio.loadmat('relaxed64.mat')['X']
        Xin = np.zeros_like(X);

        Xin[:inp_size] = X[:inp_size]*np.cos(angle) - X[inp_size:]*np.sin(angle);
        Xin[inp_size:] = X[:inp_size]*np.sin(angle) + X[inp_size:]*np.cos(angle);

        xSim = np.zeros((tsteps, inp_size))
        ySim = np.zeros((tsteps, inp_size))
        xSim[0, :] = (Xin[:inp_size] - np.mean(Xin[:inp_size])).T + x_c
        ySim[0, :] = (Xin[inp_size:] - np.mean(Xin[inp_size:])).T + y_c
        x0,y0 = xSim[0,:], ySim[0,:]
        Dx, Dy = np.real(ifft(IK * fft(x0))), np.real(ifft(IK * fft(y0)))
        r_l = np.sum(np.sqrt(Dx**2 + Dy**2))
        area_r = np.sum(-y0*Dx + x0*Dy)
        DDx, DDy = np.real(ifft(IK * fft(Dx))), np.real(ifft(IK * fft(Dy)))
        for it in xrange(tsteps-1):
            state = getState(xSim[it, :], ySim[it, :], 5e-3)
            xSim[it+1, :], ySim[it+1, :] = sim_sess.run([xNew, yNew], feed_dict={x: np.reshape(xSim[it, :], [-1, 1, inp_size]), y: np.reshape(ySim[it, :], [-1, 1, inp_size]),
                bx: state['bx'], by: state['by'], nx: state["nx"], ny: state["ny"], curvature: state["curvature"], velocity: state["v"]})
            
            f  = np.fft.rfft(xSim[it+1,:])
            f[int(2*len(f)/3):] = 0
            xSim[it+1, :] = np.fft.irfft(f, inp_size)

            f  = np.fft.rfft(ySim[it+1, :])
            f[int(2*len(f)/3):] = 0
            ySim[it+1,:] = np.fft.irfft(f, inp_size)
            xCurr = xSim[it+1, :]
            yCurr = ySim[it+1, :]
            
            def obj(x_t):
                return np.linalg.norm(x_t - np.append(xCurr, yCurr))
            def jac_obj(x_t):
                return 2*(x_t-np.append(xCurr, yCurr))
            def const_len(x_c):
                Dx = np.real(ifft(IK * fft(x_c[:inp_size])))
                Dy = np.real(ifft(IK * fft(x_c[inp_size:])))

                l   = np.sum(np.sqrt(Dx**2 + Dy**2))
                return l - r_l
            def jac_len(x_c):
                Dx = np.real(ifft(IK * fft(x_c[:inp_size])))
                Dy = np.real(ifft(IK * fft(x_c[inp_size:])))
                DDx = np.real(ifft(IK * fft(Dx)))
                DDy = np.real(ifft(IK * fft(Dy)))
                return np.append((1/np.sqrt(Dx**2+Dy**2))*Dx*DDx,(1/np.sqrt(Dx**2+Dy**2))*Dy*DDy)
            def const_area(x_c):
                Dx = np.real(ifft(IK * fft(x_c[:inp_size])))
                Dy = np.real(ifft(IK * fft(x_c[inp_size:])))
                area   = np.sum(-x_c[inp_size:]*Dx + x_c[:inp_size]*Dy)
                return area - area_r
            def jac_area(x_c):
                Dx = np.real(ifft(IK * fft(x_c[:inp_size])))
                Dy = np.real(ifft(IK * fft(x_c[inp_size:])))
                DDx = np.real(ifft(IK * fft(Dx)))
                DDy = np.real(ifft(IK * fft(Dy)))
                return np.append(-x_c[inp_size:]*DDx+Dy,-Dx+x_c[:inp_size]*DDy)

            guess = np.append(xCurr, yCurr)
            #import pdb; pdb.set_trace()
            out = scipy.optimize.minimize(obj, guess,jac=jac_obj, constraints=[{'type':'eq', 'fun':const_len, 'jac':jac_len},{'type':'eq', 'fun':const_area, 'jac':jac_area}],
                    options={"maxiter":500, "ftol": 0.5})
            xSim[it+1,:] = out["x"][:inp_size]
            ySim[it+1,:] = out["x"][inp_size:]

        sio.savemat("sim" + str(res_n) + ".mat", {'x': xSim, 'y': ySim})
        sim_time = timer() - start
        print("Simulation Finished, Time taken: " + str(sim_time))


